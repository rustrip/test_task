"use strict";
exports.__esModule = true;
// function is number even
var isNumberEven = function (value) { return Number(value) % 2 === 0; };
exports["default"] = isNumberEven;
