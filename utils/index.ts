import isNumberEven from './isNumberEven';
import isNumber from './isNumber';

export {
  isNumberEven,
  isNumber,
};