import isNumberEven from "./isNumberEven";

// is number function
const isNumber = (symbol: string): boolean => /^[1-9]\d*$/.test(symbol);

export default isNumber;