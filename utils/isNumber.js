"use strict";
exports.__esModule = true;
// is number function
var isNumber = function (symbol) { return /^[1-9]\d*$/.test(symbol); };
exports["default"] = isNumber;
