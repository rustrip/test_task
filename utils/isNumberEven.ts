// function is number even
const isNumberEven = (value: string): boolean => Number(value) % 2 === 0;

export default isNumberEven;