"use strict";
exports.__esModule = true;
exports.isNumber = exports.isNumberEven = void 0;
var isNumberEven_1 = require("./isNumberEven");
exports.isNumberEven = isNumberEven_1["default"];
var isNumber_1 = require("./isNumber");
exports.isNumber = isNumber_1["default"];
