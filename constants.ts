// regexps
const sourceRegexp1: RegExp = /^(четные|нечетные)(| )([0-9]+)-([0-9]+)(|, |; | )(четные|нечетные)(| )([0-9]+)-([0-9]+)$/;

const sourceRegexp2: RegExp = /^(четные|нечетные)(| )([0-9]+)[\+](|, |; | )(четные|нечетные)(| )([0-9]+)[\+]$/;

const sourceRegexp3: RegExp = /^(четные|нечетные) с ([0-9]+) и вся улица до конца$/;

const sourceRegexp4Chars: RegExp = /[a-zA-Zа-яА-Я]+/;

const sourceRegexp4Numbers: RegExp = /[0-9]+/;

const sourceRegexp5: RegExp = /(,)?([0-9]+)([a-zA-Zа-яА-Я]+|\/([a-zA-Zа-яА-Я]+)|)(,)?/g;

const sourceRegexp1Odd: RegExp = /(нечетные)(| )([0-9]+)-([0-9]+)/;

const sourceRegexp1Even: RegExp = /(четные)(| )([0-9]+)-([0-9]+)/;

const sourceRegexp2Odd: RegExp = /(нечетные)(| )([0-9]+)[\+]/;

const sourceRegexp2Even: RegExp = /(четные)(| )([0-9]+)[\+]/;

const sourceRegexp3Odd: RegExp = /^(нечетные)(| )с ([0-9]+)/;

const sourceRegexp3Even: RegExp = /^(четные)(| )с ([0-9]+)/;

const DASH_SYMBOL: string = '-', NEGATIVE_SYMBOL = -1, FIRST_NUMBER_INDEX: number = 3, LAST_NUMBER_INDEX: number = 4, COMMA_SYMBOL: string = ',', SPACE_SYMBOL: RegExp = / /g, EMPTY_SYMBOL: string = '';

export {
  sourceRegexp1,
  sourceRegexp2,
  sourceRegexp3,
  sourceRegexp1Odd,
  sourceRegexp1Even,
  sourceRegexp2Odd,
  sourceRegexp2Even,
  sourceRegexp3Odd,
  sourceRegexp3Even,
  sourceRegexp4Chars,
  sourceRegexp4Numbers,
  sourceRegexp5,
  DASH_SYMBOL,
  NEGATIVE_SYMBOL,
  FIRST_NUMBER_INDEX,
  LAST_NUMBER_INDEX,
  COMMA_SYMBOL,
  SPACE_SYMBOL,
  EMPTY_SYMBOL,
};