// outside modules
import { sourceRegexp1, sourceRegexp2, sourceRegexp3, sourceRegexp4Chars, sourceRegexp4Numbers, sourceRegexp5 } from './constants';
import { checkRegexp1, checkRegexp2, checkRegexp3, checkRegexp4, checkRegexp5 } from './functions';

// constants
import { DASH_SYMBOL, NEGATIVE_SYMBOL } from './constants';

// HouseParser
class HouseParser {

  constructor(private readonly source: string) {}

  public get = (): string => this.source;

  public isHouseIncluded = (houseNumber: string): boolean => {

    if (!this.source.length || !houseNumber.length) return false;

    if (houseNumber.indexOf(DASH_SYMBOL) > NEGATIVE_SYMBOL) return false;

    if (sourceRegexp1.test(this.source)) return checkRegexp1(this.source, houseNumber);

    if (sourceRegexp2.test(this.source)) return checkRegexp2(this.source, houseNumber);

    if (sourceRegexp3.test(this.source)) return checkRegexp3(this.source, houseNumber);

    if (!sourceRegexp4Chars.test(this.source) && sourceRegexp4Numbers.test(this.source)) return checkRegexp4(this.source, houseNumber);
    
    if (sourceRegexp5.test(this.source)) return checkRegexp5(this.source, houseNumber);

    return false;

  }
}

(function main() {

  const source: string = '12, 22, 36', value: string = '36';

  const houseParser = new HouseParser(source);

  console.log(houseParser.isHouseIncluded(value));

})();