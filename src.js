"use strict";
exports.__esModule = true;
// outside modules
var constants_1 = require("./constants");
var functions_1 = require("./functions");
// constants
var constants_2 = require("./constants");
// HouseParser
var HouseParser = /** @class */ (function () {
    function HouseParser(source) {
        var _this = this;
        this.source = source;
        this.get = function () { return _this.source; };
        this.isHouseIncluded = function (houseNumber) {
            if (!_this.source.length || !houseNumber.length)
                return false;
            if (houseNumber.indexOf(constants_2.DASH_SYMBOL) > constants_2.NEGATIVE_SYMBOL)
                return false;
            if (constants_1.sourceRegexp1.test(_this.source))
                return functions_1.checkRegexp1(_this.source, houseNumber);
            if (constants_1.sourceRegexp2.test(_this.source))
                return functions_1.checkRegexp2(_this.source, houseNumber);
            if (constants_1.sourceRegexp3.test(_this.source))
                return functions_1.checkRegexp3(_this.source, houseNumber);
            if (!constants_1.sourceRegexp4Chars.test(_this.source) && constants_1.sourceRegexp4Numbers.test(_this.source))
                return functions_1.checkRegexp4(_this.source, houseNumber);
            if (constants_1.sourceRegexp5.test(_this.source))
                return functions_1.checkRegexp5(_this.source, houseNumber);
            return false;
        };
    }
    return HouseParser;
}());
(function main() {
    var source = '12, 22, 36', value = '36';
    var houseParser = new HouseParser(source);
    console.log(houseParser.isHouseIncluded(value));
})();
