"use strict";
exports.__esModule = true;
// modules
var utils_1 = require("../utils");
var constants_1 = require("../constants");
// constants
var constants_2 = require("../constants");
// 1st check function
var checkRegexp2 = function (value, houseNumber) {
    try {
        if (utils_1.isNumberEven(houseNumber)) {
            var evenPart = value.match(constants_1.sourceRegexp2Even);
            if (evenPart) {
                var evenFirstNumber = evenPart[constants_2.FIRST_NUMBER_INDEX];
                if (evenFirstNumber && Number(evenFirstNumber) <= Number(houseNumber))
                    return true;
                return false;
            }
            return false;
        }
        else {
            var oddPart = value.match(constants_1.sourceRegexp2Odd);
            if (oddPart) {
                var oddFirstNumber = oddPart[constants_2.FIRST_NUMBER_INDEX];
                if (oddFirstNumber && Number(oddFirstNumber) <= Number(houseNumber))
                    return true;
                return false;
            }
            return false;
        }
    }
    catch (err) {
        console.error(err);
        return false;
    }
};
exports["default"] = checkRegexp2;
