// modules
import { isNumberEven } from '../utils';
import { sourceRegexp1Even, sourceRegexp1Odd } from '../constants';

// constants
import { FIRST_NUMBER_INDEX, LAST_NUMBER_INDEX } from '../constants';

// 1st check function
const checkRegexp1 = (value: string, houseNumber: string): boolean => {

  try {

    if (isNumberEven(houseNumber)) {

      const evenPart: RegExpMatchArray = value.match(sourceRegexp1Even);

      if (evenPart) {

        const evenFirstNumber: string = evenPart[FIRST_NUMBER_INDEX], evenLastNumber: string = evenPart[LAST_NUMBER_INDEX];

        if (evenFirstNumber && evenLastNumber && Number(evenFirstNumber) <= Number(houseNumber) && Number(evenLastNumber) >= Number(houseNumber) ) return true;

        return false;

      }

      return false;

    }

    else {

      const oddPart: RegExpMatchArray = value.match(sourceRegexp1Odd);

      if (oddPart) {

        const oddFirstNumber: string = oddPart[FIRST_NUMBER_INDEX], oddLastNumber: string = oddPart[LAST_NUMBER_INDEX];

        if (oddFirstNumber && oddLastNumber && Number(oddFirstNumber) <= Number(houseNumber) && Number(oddLastNumber) >= Number(houseNumber) ) return true;

        return false;

      }

      return false;

    }

  } catch (err) {

    console.error(err);

    return false;

  }
};

export default checkRegexp1;