// modules
import { isNumberEven } from '../utils';
import { sourceRegexp2Even, sourceRegexp2Odd } from '../constants';

// constants
import { FIRST_NUMBER_INDEX } from '../constants';

// 1st check function
const checkRegexp2 = (value: string, houseNumber: string): boolean => {

  try {

    if (isNumberEven(houseNumber)) {

      const evenPart: RegExpMatchArray = value.match(sourceRegexp2Even);

      if (evenPart) {

        const evenFirstNumber: string = evenPart[FIRST_NUMBER_INDEX];

        if (evenFirstNumber && Number(evenFirstNumber) <= Number(houseNumber) ) return true;

        return false;

      }

      return false;

    }

    else {

      const oddPart: RegExpMatchArray = value.match(sourceRegexp2Odd);

      if (oddPart) {

        const oddFirstNumber: string = oddPart[FIRST_NUMBER_INDEX];

        if (oddFirstNumber && Number(oddFirstNumber) <= Number(houseNumber) ) return true;

        return false;

      }

      return false;

    }

  } catch (err) {

    console.error(err);

    return false;

  }
};

export default checkRegexp2;