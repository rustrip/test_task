"use strict";
exports.__esModule = true;
// constants
var constants_1 = require("../constants");
// 1st check function
var checkRegexp4 = function (value, houseNumber) {
    try {
        var numberArray = value.replace(constants_1.SPACE_SYMBOL, constants_1.EMPTY_SYMBOL).split(constants_1.COMMA_SYMBOL);
        return numberArray.indexOf(houseNumber) > -1;
    }
    catch (err) {
        console.error(err);
        return false;
    }
};
exports["default"] = checkRegexp4;
