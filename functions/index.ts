import checkRegexp1 from './checkRegexp1';
import checkRegexp2 from './checkRegexp2';
import checkRegexp3 from './checkRegexp3';
import checkRegexp4 from './checkRegexp4';
import checkRegexp5 from './checkRegexp5';

export {
  checkRegexp1,
  checkRegexp2,
  checkRegexp3,
  checkRegexp4,
  checkRegexp5,
};