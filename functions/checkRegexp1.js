"use strict";
exports.__esModule = true;
// modules
var utils_1 = require("../utils");
var constants_1 = require("../constants");
// constants
var constants_2 = require("../constants");
// 1st check function
var checkRegexp1 = function (value, houseNumber) {
    try {
        if (utils_1.isNumberEven(houseNumber)) {
            var evenPart = value.match(constants_1.sourceRegexp1Even);
            if (evenPart) {
                var evenFirstNumber = evenPart[constants_2.FIRST_NUMBER_INDEX], evenLastNumber = evenPart[constants_2.LAST_NUMBER_INDEX];
                if (evenFirstNumber && evenLastNumber && Number(evenFirstNumber) <= Number(houseNumber) && Number(evenLastNumber) >= Number(houseNumber))
                    return true;
                return false;
            }
            return false;
        }
        else {
            var oddPart = value.match(constants_1.sourceRegexp1Odd);
            if (oddPart) {
                var oddFirstNumber = oddPart[constants_2.FIRST_NUMBER_INDEX], oddLastNumber = oddPart[constants_2.LAST_NUMBER_INDEX];
                if (oddFirstNumber && oddLastNumber && Number(oddFirstNumber) <= Number(houseNumber) && Number(oddLastNumber) >= Number(houseNumber))
                    return true;
                return false;
            }
            return false;
        }
    }
    catch (err) {
        console.error(err);
        return false;
    }
};
exports["default"] = checkRegexp1;
