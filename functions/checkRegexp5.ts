// constants
import { COMMA_SYMBOL, SPACE_SYMBOL, EMPTY_SYMBOL } from '../constants';

// 1st check function
const checkRegexp5 = (value: string, houseNumber: string): boolean => {

  try {

    const numberArray: string[] = value.replace(SPACE_SYMBOL, EMPTY_SYMBOL).split(COMMA_SYMBOL);

    return numberArray.indexOf(houseNumber) > -1;

  } catch (err) {

    console.error(err);

    return false;

  }
};

export default checkRegexp5;