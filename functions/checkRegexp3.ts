// modules
import { isNumberEven } from '../utils';
import { sourceRegexp3Even, sourceRegexp3Odd } from '../constants';

// constants
import { FIRST_NUMBER_INDEX } from '../constants';

// 1st check function
const checkRegexp3 = (value: string, houseNumber: string): boolean => {

  try {

    if (isNumberEven(houseNumber)) {

      const evenPart: RegExpMatchArray = value.match(sourceRegexp3Even);

      if (evenPart) {

        const evenFirstNumber: string = evenPart[FIRST_NUMBER_INDEX];

        if (evenFirstNumber && Number(evenFirstNumber) <= Number(houseNumber) ) return true;

        return false;

      }

      return false;

    }

    else {

      const oddPart: RegExpMatchArray = value.match(sourceRegexp3Odd);

      if (oddPart) {

        const oddFirstNumber: string = oddPart[FIRST_NUMBER_INDEX];

        if (oddFirstNumber && Number(oddFirstNumber) <= Number(houseNumber) ) return true;

        return false;

      }

      return false;

    }

  } catch (err) {

    console.error(err);

    return false;

  }
};

export default checkRegexp3;